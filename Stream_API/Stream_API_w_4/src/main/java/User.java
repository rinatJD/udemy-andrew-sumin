import lombok.*;

@Data
@AllArgsConstructor
@Getter
@Setter
public class User {
    private String name;
    private int age;
}
