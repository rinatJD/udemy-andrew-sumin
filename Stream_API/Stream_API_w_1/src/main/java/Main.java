import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Main {
    public static void main(String[] args) {

        List<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < 1000; i++) {
            numbers.add((int)(Math.random() * 100 + 100));
        }

        numbers.stream().filter(x -> x % 2 == 0).map(x -> "number: " + x).filter(x -> x.endsWith("0")).map(x -> x + "!").forEach(System.out::println);

    }

    private static List<Integer> filter (List<Integer> numbers, Predicate predicate) {

        List<Integer> result = new ArrayList<>();

        for (int num : numbers) {

            if (predicate.test(num)) {
                result.add(num);
            }
        }

        return result;

    };
}
