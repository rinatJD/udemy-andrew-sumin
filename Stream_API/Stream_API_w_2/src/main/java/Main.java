import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        List<Integer> numbers = new ArrayList<>();

        for (int i = 0; i < 1000; i++) {
            numbers.add((int)(Math.random() * 100 + 100));
        }

        List<String> result = numbers.stream()
                .filter(x -> (x % 5 == 0) && (x % 2 == 0))
                .map(x -> Math.sqrt(x))
                .map(x -> "Корень: " + x)
                .collect(Collectors.toList());

        result.forEach(System.out::println);

    }

}
