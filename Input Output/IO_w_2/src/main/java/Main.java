import java.io.*;

public class Main {
    public static void main(String[] args) {

        File directory = new File("folder");
        File file = new File(directory, "file.txt");

        directory.mkdir();
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (OutputStream outputStream = new FileOutputStream(file)) {

            byte[] bytes = {'i', 'v', 'a', 'n'};
            outputStream.write(bytes);

        } catch (IOException exception) {
            exception.printStackTrace();
        }

        try (InputStream inputStream = new FileInputStream(file)) {

            int a = inputStream.read();

            while (a != -1) {
                System.out.print((char) a);
                a = inputStream.read();
            }

        }
        catch (IOException exception) {
            exception.printStackTrace();
        }

    }
}
