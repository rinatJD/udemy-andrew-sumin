import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        File directory = new File("folder/folder1/folder2");
        File file = new File("folder/folder1/folder2/file1.txt");
        File Afile1 = new File("folder/folder1/folder2/Afile1.txt");
        File Afile4 = new File("folder/folder1/folder2/Afile4.txt");

        try {
            directory.mkdirs();
            file.createNewFile();
            Afile1.createNewFile();
            Afile4.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File[] files = directory.listFiles((dir, name) -> name.startsWith("A"));

        Arrays.stream(files).forEach(x -> System.out.println(x.getAbsolutePath()));

    }
}
