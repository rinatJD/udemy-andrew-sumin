import java.io.*;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        File directory = new File("folder");
        File file = new File(directory, "names.txt");

        directory.mkdir();
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (Reader reader = new InputStreamReader(new FileInputStream(file))){

            char[] array = new char[10];
            StringBuilder stringBuilder = new StringBuilder();
            int count = reader.read(array);

            while (count > 0) {
                stringBuilder.append(array, 0, count);
                count = reader.read(array);
            }

            String string = new String(stringBuilder);

            String[] arr = string.split(" ");

            Arrays.stream(arr)
                    .filter(x -> x.startsWith("А"))
                    .forEach(System.out::println);


        } catch (IOException e) {
            e.printStackTrace();
        }



    }
}
