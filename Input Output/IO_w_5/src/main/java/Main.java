import java.io.File;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        File file = new File("file.txt");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter next page:");

        try (RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r")) {

            String next = scanner.nextLine();

            while (!next.equals("stop")) {

                byte[] array = new byte[3000];
                randomAccessFile.seek((Integer.parseInt(next)-1) * 3000L);
                int count = randomAccessFile.read(array);
                System.out.println(new String(array, 0, count));
                next = scanner.nextLine();

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
