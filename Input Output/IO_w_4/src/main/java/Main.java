import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        File file = new File("names.txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter name:");
        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file, true))) {
            String name = scanner.nextLine();
            while (!name.equals("Stop")) {
                outputStream.write(name.getBytes(StandardCharsets.UTF_8));
                outputStream.write('\n');
                outputStream.flush();
                name = scanner.nextLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (InputStream inputStream = new BufferedInputStream(new FileInputStream(file))) {
            byte[] bytes = new byte[128];
            int count = inputStream.read(bytes);
            StringBuilder stringBuilder = new StringBuilder();
            while (count > 0) {
                stringBuilder.append(new String(bytes, 0, count));
                count = inputStream.read(bytes);
            }
            System.out.println(stringBuilder.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
